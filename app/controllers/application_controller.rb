class ApplicationController < ActionController::Base
  protect_from_forgery
  include SessionsHelper
  def signed_in_user
    unless session[:id]
        flash[:notice] = "Please log in"
        redirect_to :root
        return false
    else
        return true
    end
end
end
