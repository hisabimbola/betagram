class UsersController < ApplicationController
  before_filter :signed_in_user, only: [:edit, :update]
  def show
  	@user = User.find(params[:id])
  end

  def new
  	@user = User.new
  end

  def create
  	@user = User.new(params[:user])
  	if @user.save
      sign_in @user
  		flash[:success] = "Welcome to BetaGram, The free Online English Test"
  	  	#Handle a successful save
        redirect_to @user
  	  else
  	  	render 'new'
  	  end
  end

  def signed_in_user
    redirect_to sign_in_path, notice: "Please sign in."     unless signed_in?
  end
end
