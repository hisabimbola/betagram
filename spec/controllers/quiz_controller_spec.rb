require 'spec_helper'

describe QuizController do

  describe "GET 'start'" do
    it "returns http success" do
      get 'start'
      response.should be_success
    end
  end

  describe "GET 'end'" do
    it "returns http success" do
      get 'end'
      response.should be_success
    end
  end

end
