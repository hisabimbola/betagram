require 'spec_helper'

describe "choices/edit" do
  before(:each) do
    @choice = assign(:choice, stub_model(Choice))
  end

  it "renders the edit choice form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => choices_path(@choice), :method => "post" do
    end
  end
end
