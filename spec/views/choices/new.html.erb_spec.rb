require 'spec_helper'

describe "choices/new" do
  before(:each) do
    assign(:choice, stub_model(Choice).as_new_record)
  end

  it "renders new choice form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => choices_path, :method => "post" do
    end
  end
end
